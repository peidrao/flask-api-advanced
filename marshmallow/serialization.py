from marshmallow import Schema, fields


class BookSchema(Schema):
    name = fields.Str()
    author = fields.Str()
    description = fields.Str()


class Book:
    def __init__(self, name: str, author: str, description: str) -> None:
        self.name = name
        self.author = author
        self.description = description


book = Book("Clean Code", "Bob Martin", "A book about writing cleaner code.")
book_schema = BookSchema()
book_dict = book_schema.dump(book)
# print(book_dict)


# --------------------------
serializer_book = {
    "name": "Clean Code",
    "author": "Bob Martin",
    "description": "A book about writing cleaner code.",
}
book_schema = BookSchema()
book = book_schema.load(serializer_book)
book_obj = Book(**book)

print(book_obj.name)
