from marshmallow_serializer import ma
from models.confirmation import ConfirmationModel


class ConfirmationSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ConfirmationModel
        load_only = ("user",)
        load_only = ("id", "expire_at", "confirmed")
        include_fk = True
