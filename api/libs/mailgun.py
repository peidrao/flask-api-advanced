from typing import List
from requests import Response, post
from settings import MAILGUN_API_KEY, MAILGUN_DOMAIN, MAILGUN_FROM


class MailGunException(Exception):
    def __init__(self, message: str):
        super().__init__(message)


def send_email(email: List[str], subject: str, text: str, html: str) -> Response:
    if not MAILGUN_API_KEY:
        raise MailGunException("failed to load mailgun API Key")

    if not MAILGUN_DOMAIN:
        raise MailGunException("failed to load mailgun domain")

    response = post(
        MAILGUN_DOMAIN,
        auth=("api", MAILGUN_API_KEY),
        data={
            "from": MAILGUN_FROM,
            "to": email,
            "subject": subject,
            "text": text,
            "html": html,
        },
    )

    if response.status_code != 200:
        raise MailGunException(
            "Error in sending confirmation email, user registration failed."
        )

    return response
