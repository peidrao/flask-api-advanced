BLANK_ERROR = "{} cannot be blank."
NOT_FOUND = "{} not found."
DELETED = "{} deleted."
ERROR_INSERTING = "An error occurred while inserting the {}"
NAME_ALREADY_EXISTS = "An {} with name {} already exists."
CREATED_SUCCESS = "{} created successfully."
USER_ALREADY_EXISTS = "A user with that username already exists."
INVALID_CREDENTIALS = "Invalid credentials."
NOT_CONFIRMED_ERROR = (
    "You have not confirmed registration, please check your email <{}>"
)
USER_CONFIRMED = "User confirmed"
