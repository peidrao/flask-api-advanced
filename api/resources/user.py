import traceback
from flask_restful import Resource
from hmac import compare_digest
from flask import request, g
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    get_jwt_identity,
    jwt_required,
    get_jwt,
)
from models.user import UserModel
from schemas.user import UserSchema
from blocklist import BLOCKLIST
from libs.mailgun import MailGunException
from constants_messages import (
    CREATED_SUCCESS,
    DELETED,
    INVALID_CREDENTIALS,
    NOT_FOUND,
    USER_ALREADY_EXISTS,
    NOT_CONFIRMED_ERROR,
)
from models.confirmation import ConfirmationModel
from libs.test_flask_lib import function_accessing_global


user_schema = UserSchema()


class UserRegister(Resource):
    def post(self):
        user = UserSchema().load(request.get_json())

        if UserModel.find_by_username(user.username):
            return {"message": USER_ALREADY_EXISTS}, 400
        try:
            user.save_to_db()
            confirmation = ConfirmationModel(user.id)
            confirmation.save_to_db()
            user.send_confirmation_email()
        except MailGunException as e:
            user.delete_from_db()
            return {"message": str(e)}, 500
        except:
            traceback.print_exc()
            user.delete_from_db()
            return {"message": "Failed to create"}, 500

        return {"message": CREATED_SUCCESS.format("User")}, 201


class User(Resource):
    @classmethod
    def get(cls, user_id: int):
        user = UserModel.find_by_id(user_id)
        if not user:
            return {"message": NOT_FOUND.format("User")}, 404
        return UserSchema().dump(user), 200

    @classmethod
    def delete(cls, user_id: int):
        user = UserModel.find_by_id(user_id)
        if not user:
            return {"message": NOT_FOUND.format("User")}, 404
        user.delete_from_db()
        return {"message": DELETED.format("User")}, 200


class UserLogin(Resource):
    def post(self):
        data = UserSchema().load(request.get_json())
        user = UserModel.find_by_username(data.username)
        g.token = "Test token"
        function_accessing_global()
        if user and compare_digest(user.password, data.password):
            confirmation = user.most_recent_confirmation
            if confirmation and confirmation.confirmed:
                access_token = create_access_token(identity=user.id, fresh=True)
                refresh_token = create_refresh_token(user.id)
                return {
                    "access_token": access_token,
                    "refresh_token": refresh_token,
                }, 200
            return {"message": NOT_CONFIRMED_ERROR.format(user.username)}

        return {"message": INVALID_CREDENTIALS}, 401


class UserLogout(Resource):
    @jwt_required()
    def post(self):
        # jti is "JWT ID", a unique identifier for a JWT.
        jti = get_jwt()["jti"]
        user_id = get_jwt_identity()
        BLOCKLIST.add(jti)
        return {"message": "User <id={}> successfully logged out.".format(user_id)}, 200


class TokenRefresh(Resource):
    @jwt_required(refresh=True)
    def post(self):
        current_user = get_jwt_identity()
        new_token = create_access_token(identity=current_user, fresh=False)
        return {"access_token": new_token}, 200


class SetPassword(Resource):
    @classmethod
    @jwt_required(fresh=True)
    def post(cls):
        user_json = request.get_json()


        user_data = UserSchema().load(user_json)
        user = UserModel.find_by_username(user_data.username)

        if not user:
            return {"message": "user not found"}, 400

        user.password = user_data.password
        user.save_to_db()
        return {"message": "Password updated"}, 200
