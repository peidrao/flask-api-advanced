import traceback
import os
from flask_restful import Resource
from flask_uploads import UploadNotAllowed
from flask import request, send_file
from flask_jwt_extended import jwt_required, get_jwt_identity


from libs import image_helper
from schemas.image import ImageSchema


class ImageUpload(Resource):
    @jwt_required()
    def post(self):
        data = ImageSchema().load(request.files)
        user_id = get_jwt_identity()
        folder = f"user_{user_id}"
        try:
            image_path = image_helper.save_image(data["image"], folder=folder)
            basename = image_helper.get_basename(image_path)
            return {"message": f"Image upload: {basename}"}, 201
        except UploadNotAllowed:
            extension = image_helper.get_extension(data["image"])
            return {"message": f"Image illegal extension {extension}"}, 400


class Image(Resource):
    @jwt_required()
    def get(self, filename: str):
        user_id = get_jwt_identity()
        folder = f"user_{user_id}"
        if not image_helper.is_filename_safe(filename):
            return {"message": "Image illegal file name"}, 400

        try:
            return send_file(image_helper.get_path(filename, folder=folder))
        except FileNotFoundError:
            return {"message": "Image not found"}, 404

    @jwt_required()
    def delete(self, filename: str):
        user_id = get_jwt_identity()
        folder = f"user_{user_id}"
        if not image_helper.is_filename_safe(filename):
            return {"message": "Image illegal file name"}, 400

        try:
            os.remove(image_helper.get_path(filename, folder=folder))
            return {"message": "Image deleted"}, 200
        except FileNotFoundError:
            return {"message": "Image not found"}, 404
        except:
            traceback.print_exc()
            return {"message": "Image delete failed"}


class AvatarUpload(Resource):
    @jwt_required()
    def put(self):
        data = ImageSchema().load(request.files)
        filename = f"user_{get_jwt_identity()}"
        folder = "avatars"
        avatar_path = image_helper.find_image_any_format(filename, folder)
        if avatar_path:
            try:
                os.remove(avatar_path)
            except:
                return {"message": "Avatar delete failed"}, 400
        try:
            ext = image_helper.get_extension(data["image"].filename)
            avatar = filename + ext
            avatar_path = image_helper.save_image(
                data["image"], folder=folder, name=avatar
            )
            return {"message": "avatar uploaded "}, 200
        except FileNotFoundError:
            return {"message": "Image not found"}, 404


class Avatar(Resource):
    @classmethod
    @jwt_required()
    def get(self):
        folder = "avatars"
        filename = f"user_{get_jwt_identity()}"
        avatar = image_helper.find_image_any_format(filename, folder)
        if avatar:
            return send_file(avatar)

        try:
            return send_file(image_helper.get_path(filename, folder=folder))
        except FileNotFoundError:
            return {"message": "avatar not found"}, 404
