from flask_restful import Resource
from flask_jwt_extended import jwt_required
from models.item import ItemModel
from flask import request
from schemas.item import ItemSchema

from constants_messages import (
    BLANK_ERROR,
    DELETED,
    ERROR_INSERTING,
    NAME_ALREADY_EXISTS,
    NOT_FOUND,
)


class Item(Resource):
    def get(self, name: str):
        item = ItemModel.find_by_name(name)
        if item:
            return ItemSchema().dump(item)
        return {"message": NOT_FOUND.format("Item")}, 404

    @jwt_required(fresh=True)
    def post(self, name: str):
        if ItemModel.find_by_name(name):
            return {"message": NAME_ALREADY_EXISTS.format("item", name)}, 400

        item_json = request.get_json()
        item_json["name"] = name

        item = ItemSchema().load(item_json)

        try:
            item.save_to_db()
        except:
            return {"message": ERROR_INSERTING.format("item")}, 500

        return ItemSchema().dump(item), 201

    @jwt_required()
    def delete(self, name: str):
        item = ItemModel.find_by_name(name)
        if item:
            item.delete_from_db()
            return {"message": DELETED.format("Item")}, 200
        return {"message": NOT_FOUND.format("Item")}, 404

    def put(self, name: str):
        data = request.get_json()

        item = ItemModel.find_by_name(name)

        if item:
            item.price = data["price"]
        else:
            data["name"] = name
        item = ItemSchema().load(data)

        item.save_to_db()

        return ItemSchema().dump(item), 200


class ItemList(Resource):
    def get(self):
        return {"items": ItemSchema(many=True).dump(ItemModel.find_all())}, 200
        # return {"items": [ItemSchema().dump(item) for item in ItemModel.find_all()]}, 200
