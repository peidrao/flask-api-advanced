from flask_restful import Resource
from models.store import StoreModel
from schemas.store import StoreSchema

from constants_messages import DELETED, ERROR_INSERTING, NAME_ALREADY_EXISTS, NOT_FOUND


class Store(Resource):
    def get(self, name: str):
        store = StoreModel.find_by_name(name)
        if store:
            return StoreSchema().dump(store), 200
        return {"message": NOT_FOUND.format("Store")}, 404

    def post(self, name: str):
        if StoreModel.find_by_name(name):
            return {"message": NAME_ALREADY_EXISTS.format("store", name)}, 400

        store = StoreModel(name=name)
        try:
            store.save_to_db()
        except:
            return {"message": ERROR_INSERTING.format("store")}, 500

        return StoreSchema().dump(store), 201

    def delete(self, name: str):
        store = StoreModel.find_by_name(name)
        if store:
            store.delete_from_db()

        return {"message": DELETED.format("Store")}


class StoreList(Resource):
    def get(self):
        return {"stores": StoreSchema(many=True).dump(StoreModel.find_all())}, 200
